pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.6.1-jdk-8-slim'
                }
            }
            steps {
                script {
                    sh 'mvn clean package -B -ntp -DskipTests'
                }
            }
        }
        stage('Testing') {
            agent  {
                docker{
                    image 'maven:3.6.1-jdk-8-slim'
                }
            }
            steps {
                script {
                    sh 'mvn test'
                }
            }
        }
        stage('Sonar') {
            agent any
            steps{
                withCredentials([[$class: 'FileBinding', credentialsId: 'sonarqube-settings', variable:'M2_SETTINGS']]){
                    sh "mvn sonar:sonar -B -s ${M2_SETTINGS}"
                }
            }
        }
        stage('Deploy') {
            agent any
            steps {
                script {
                    sh "ls -l"
                    def pom = readMavenPom file : 'pom.xml'
                    println pom

                    def app = docker.build("sleyter94/${pom.artifactId}:${pom.version}")

                    docker.withRegistry('https://registry.hub.docker.com/', 'dockerhub-credentials'){
                        app.push()
                        app.push('latest')
                    }
                }
            }
        }
    }
    post {
        failure {
            emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
                    recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                    subject: "Jenkins Failed ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
        }

        success {
            emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
            recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
            subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
        }
    }
}