package com.mitocode;

import com.mitocode.model.Estudiante;
import com.mitocode.rest.EstudianteRest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ApiStudentMicroserviceApplicationTests {

	private Estudiante estudiante;
	private EstudianteRest estudianteRest;

	@BeforeEach
	void init(){
		estudiante = new Estudiante();
		estudiante.setApellidos("Sandoval Vilcapoma");
		estudiante.setEdad(26);
		estudiante.setId(1L);
		estudiante.setNombres("Sleyter Oliver");
		estudianteRest = new EstudianteRest();
	}

	@Test
	void validateGet(){
		assertEquals(estudiante.getNombres(),"Sleyter Oliver");
		assertEquals(estudiante.getApellidos(),"Sandoval Vilcapoma");
		assertEquals(estudiante.getId(),1L);
		assertEquals(estudiante.getEdad(),26);
	}

	@Test
	void validateRestService(){
		assertEquals(estudianteRest.listarTodos().size(),2);
	}

	@Test
	void contextLoads() {
	}

}
