# Getting Started

### Reference Documentation
El Microservicio lista, busca y agrega estudiantes.
 
 El despliegue tiene 4 fases:
 1. Build -> Se construye en un contenedor de docker
 2. Testing -> Se ejecutan los test del proyecto, generando el reporte de jacoco
 3. Sonar -> Se ejecuta el sonar, llevando los resultados a la instancia del sonar
 4. Deploy -> Una vez construido el proyecto, se construye la imagen y se envia al docker hub